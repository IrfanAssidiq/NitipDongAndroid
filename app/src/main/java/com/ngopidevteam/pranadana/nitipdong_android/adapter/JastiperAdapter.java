package com.ngopidevteam.pranadana.nitipdong_android.adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.allactivity.DetailPurchaseAct;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Jastiper;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class JastiperAdapter extends RecyclerView.Adapter<JastiperAdapter.ViewHolder> {

    private List<Jastiper> jastiperList;
    private Context ctx;

    public JastiperAdapter(List<Jastiper> jastiperList, Context ctx) {
        this.jastiperList = jastiperList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jastiper_profile, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Jastiper jastiper = jastiperList.get(position);

        holder.imgProdukJastiper.setImageResource(jastiper.getJastiperImage());
        holder.txtNamaProduk.setText(jastiper.getNama());

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String harga = jastiper.getHarga();
        long rupiah = Long.parseLong(harga);
        holder.txtHargaProduk.setText(formatRupiah.format(rupiah));

        holder.imgProdukJastiper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(holder.itemView.getContext(), ""+holder.txtNamaProduk.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(holder.itemView.getContext(), ""+holder.txtNamaProduk.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        holder.grid_jastiper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ctx.startActivity(new Intent(ctx, DetailPurchaseAct.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return jastiperList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgProdukJastiper;
        private TextView txtNamaProduk, txtHargaProduk;
        private GridLayout grid_jastiper;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            grid_jastiper = itemView.findViewById(R.id.grid_jastiper);
            imgProdukJastiper = itemView.findViewById(R.id.img__jastiper_produk);
            txtNamaProduk = itemView.findViewById(R.id.txt__jastiper_produk);
            txtHargaProduk = itemView.findViewById(R.id.hrg_jastiper_produk);
        }
    }
}
