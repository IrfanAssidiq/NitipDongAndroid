package com.ngopidevteam.pranadana.nitipdong_android.fragment;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.adapter.ProfilAdapter;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.DetailProfil;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.ListDetailProfil;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailAkunFragment extends Fragment {

    private RecyclerView rvDetailProfil;
    private ProfilAdapter profilAdapter;

    public DetailAkunFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_akun, container, false);

        rvDetailProfil = view.findViewById(R.id.rec_detail_profil);
        rvDetailProfil.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        profilAdapter = new ProfilAdapter(getContext());
        rvDetailProfil.setAdapter(profilAdapter);
        setData();
    }

    private void setData() {
        List<DetailProfil> data = ListDetailProfil.detailProfilList();
        profilAdapter.updateData(data);
    }

}
