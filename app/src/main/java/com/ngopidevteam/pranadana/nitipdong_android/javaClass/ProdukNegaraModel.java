package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

public class ProdukNegaraModel {
    int imgProdukNegara;
    String namaProdukNegara, hargaProdukNegara;

    public int getImgProdukNegara() {
        return imgProdukNegara;
    }

    public void setImgProdukNegara(int imgProdukNegara) {
        this.imgProdukNegara = imgProdukNegara;
    }

    public String getNamaProdukNegara() {
        return namaProdukNegara;
    }

    public void setNamaProdukNegara(String namaProdukNegara) {
        this.namaProdukNegara = namaProdukNegara;
    }

    public String getHargaProdukNegara() {
        return hargaProdukNegara;
    }

    public void setHargaProdukNegara(String hargaProdukNegara) {
        this.hargaProdukNegara = hargaProdukNegara;
    }

    public ProdukNegaraModel(int imgProdukNegara, String namaProdukNegara, String hargaProdukNegara) {
        this.imgProdukNegara = imgProdukNegara;
        this.namaProdukNegara = namaProdukNegara;
        this.hargaProdukNegara = hargaProdukNegara;
    }

    public ProdukNegaraModel() {
    }
}
