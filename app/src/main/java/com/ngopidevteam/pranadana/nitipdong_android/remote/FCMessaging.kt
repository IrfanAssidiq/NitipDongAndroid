package com.ngopidevteam.pranadana.nitipdong_android.remote

import android.util.Log.e
import com.google.firebase.messaging.FirebaseMessagingService
import com.ngopidevteam.pranadana.nitipdong_android.utils.SharedPrefManager


/**
 *   created by Irfan Assidiq on 2020-02-20
 *   email : assidiq.irfan@gmail.com
 **/
class FCMessaging : FirebaseMessagingService() {
    lateinit var prefsHelper : SharedPrefManager
    private var NITIPCHANNEL  = "nitipdongApps"

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        prefsHelper = SharedPrefManager(this)
        e("TAGTOKEN", p0);
        prefsHelper.gToken = p0

    }
}