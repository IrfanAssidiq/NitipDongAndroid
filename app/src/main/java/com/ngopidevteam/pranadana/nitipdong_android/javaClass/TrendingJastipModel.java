package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

public class TrendingJastipModel {
    int img_trending;
    String nama, harga;

    public TrendingJastipModel(int img_trending, String nama, String harga) {
        this.img_trending = img_trending;
        this.nama = nama;
        this.harga = harga;
    }

    public int getImg_trending() {
        return img_trending;
    }

    public void setImg_trending(int img_trending) {
        this.img_trending = img_trending;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}
