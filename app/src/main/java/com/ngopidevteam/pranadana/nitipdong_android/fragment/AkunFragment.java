package com.ngopidevteam.pranadana.nitipdong_android.fragment;


import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ngopidevteam.pranadana.nitipdong_android.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AkunFragment extends Fragment {

    Button btnDetailProfile, btnJastiperProfile;
    LinearLayout llShowHide;
    Fragment frag = null;

    public AkunFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_akun, container, false);

        loadFragment(new DetailAkunFragment());

        llShowHide = view.findViewById(R.id.ll_rating_jastiper);
        btnDetailProfile = view.findViewById(R.id.profile_self);
        btnJastiperProfile = view.findViewById(R.id.profile_jastiper);

        loadFragment(new DetailAkunFragment());

        btnDetailProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag = new DetailAkunFragment();
                llShowHide.setVisibility(View.GONE);
                loadFragment(frag);
                btnDetailProfile.setTextColor(getResources().getColor(R.color.putih));
                btnJastiperProfile.setTextColor(getResources().getColor(R.color.biru));
                btnDetailProfile.setBackground(getResources().getDrawable(R.drawable.btn_rounded_orange));
                btnJastiperProfile.setBackground(getResources().getDrawable(R.drawable.btn_round));
            }
        });

        btnJastiperProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag = new JastiperFragment();
                llShowHide.setVisibility(View.VISIBLE);
                loadFragment(frag);
                btnDetailProfile.setTextColor(getResources().getColor(R.color.orange));
                btnJastiperProfile.setTextColor(getResources().getColor(R.color.putih));
                btnDetailProfile.setBackground(getResources().getDrawable(R.drawable.btn_round));
                btnJastiperProfile.setBackground(getResources().getDrawable(R.drawable.btn_rounded_biru));
            }
        });

        return view;
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null){
            getChildFragmentManager().beginTransaction().replace(R.id.frame_akun, fragment).commit();
        }
        return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Window window = getActivity().getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.putih));
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }
}
