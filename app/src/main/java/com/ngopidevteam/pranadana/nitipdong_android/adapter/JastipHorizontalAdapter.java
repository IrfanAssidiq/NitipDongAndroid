package com.ngopidevteam.pranadana.nitipdong_android.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ngopidevteam.pranadana.nitipdong_android.allactivity.DetailPurchaseAct;
import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Jastip;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class JastipHorizontalAdapter extends RecyclerView.Adapter<JastipHorizontalAdapter.JastipViewHolder> {
    private List<Jastip> horizontalJastip;
    Context context;

    public JastipHorizontalAdapter(List<Jastip> horizontalJastip, Context context) {
        this.horizontalJastip = horizontalJastip;
        this.context = context;
    }

    @NonNull
    @Override
    public JastipViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View productView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_jastip, viewGroup, false);
        JastipViewHolder jvh = new JastipViewHolder(productView);
        return jvh;
    }

    @Override
    public void onBindViewHolder(@NonNull JastipViewHolder jastipViewHolder, final int i) {
        jastipViewHolder.imgbtn.setImageResource(horizontalJastip.get(i).getGambarProduk());
        jastipViewHolder.txtProduk.setText(horizontalJastip.get(i).getNamaProduk());

        String harga = horizontalJastip.get(i).getHargaProduk();
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        long rupiah = Long.parseLong(harga);
        jastipViewHolder.txtHarga.setText(formatRupiah.format(rupiah));

        jastipViewHolder.imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String namaProduk = horizontalJastip.get(i).getNamaProduk().toString();
                Toast.makeText(context, namaProduk + " dipilih", Toast.LENGTH_SHORT).show();
                context.startActivity(new Intent(context, DetailPurchaseAct.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return horizontalJastip.size();
    }

    public class JastipViewHolder extends RecyclerView.ViewHolder{
        ImageButton imgbtn;
        TextView txtProduk, txtHarga;

        public JastipViewHolder(@NonNull View itemView) {
            super(itemView);
            imgbtn = itemView.findViewById(R.id.img_produk);
            txtProduk = itemView.findViewById(R.id.txt_produk);
            txtHarga = itemView.findViewById(R.id.hrg_produk);
        }
    }
}
