package com.ngopidevteam.pranadana.nitipdong_android.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.adapter.JastiperAdapter;
import com.ngopidevteam.pranadana.nitipdong_android.allactivity.IdCheckActivity;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Jastiper;
import com.ngopidevteam.pranadana.nitipdong_android.utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class JastiperFragment extends Fragment {

    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<Jastiper> jastiperList;
    private LinearLayout llayout;
    private SharedPrefManager prefsHelper;
    private ToggleButton toggleButton;

    public JastiperFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jastiper_akun, container, false);

        recyclerView = view.findViewById(R.id.rec_jastiper);
        llayout = view.findViewById(R.id.llayout);
        toggleButton = view.findViewById(R.id.toggleButton);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        prefsHelper = new SharedPrefManager(getActivity().getApplicationContext());

        boolean isJastiper = prefsHelper.getIsJASTIPER();
        if(isJastiper){
            recyclerView.setVisibility(View.VISIBLE);
            llayout.setVisibility(View.GONE);
            jastiperList = new ArrayList<>();
            jastiperList.add(new Jastiper(R.drawable.tas_pertama, "Tas 1", "500000"));
            jastiperList.add(new Jastiper(R.drawable.tas_kedua, "Tas 2", "750000"));
            jastiperList.add(new Jastiper(R.drawable.tas_ketiga, "Tas 3", "1000000"));
            jastiperList.add(new Jastiper(R.drawable.tas_keempat, "Tas 4", "300000"));
            jastiperList.add(new Jastiper(R.drawable.tas_kelima, "Tas 5", "500000"));
            adapter = new JastiperAdapter(jastiperList, getContext());
            recyclerView.setAdapter(adapter);
        }

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    Toast.makeText(getContext(), "is checked", Toast.LENGTH_SHORT).show();
                    toggleButton.setBackgroundColor(getResources().getColor(R.color.biru));
                    startActivity(new Intent(getContext(), IdCheckActivity.class));
//                    getActivity().finish();
                    prefsHelper.setIsJASTIPER(true);
                }else{
                    toggleButton.setBackgroundColor(getResources().getColor(R.color.orange));
                    Toast.makeText(getContext(), "is not Checked", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}
