package com.ngopidevteam.pranadana.nitipdong_android.allactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIClient;
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIService;
import com.ngopidevteam.pranadana.nitipdong_android.utils.SharedPrefManager;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity implements InputFilter {

    EditText etNama, etUsername, etEmail, etPass, etConfPass, etAddress, etAge, etGender;
    Button btnRegister;
    FirebaseAuth firebaseAuth;
    TextView txtLogin;
    Spinner spAge;

    Context mContext;
    ProgressDialog progressDialog;
    APIService apiService;
    SharedPrefManager prefshelper;

    private int min, max;

    public Register() {
    }

    public Register(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public Register(String min, String max){
        this.min = Integer.parseInt(min);
        this.max = Integer.parseInt(max);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etNama = findViewById(R.id.reg_name);
        etUsername = findViewById(R.id.reg_username);
        etEmail = findViewById(R.id.reg_email);
        etPass = findViewById(R.id.reg_pass);
        etConfPass = findViewById(R.id.reg_confirm);
        etAddress = findViewById(R.id.reg_address);
        etAge = findViewById(R.id.reg_age);
        etAge.setFilters(new InputFilter[]{
                new Register("1", "99")
        });
//        etGender = findViewById(R.id.reg_gender);
        spAge = findViewById(R.id.spin_gender);

        btnRegister = findViewById(R.id.btn_reg);

        mContext = this;
        prefshelper = new SharedPrefManager(this);
        apiService = APIClient.getClient().create(APIService.class);
        progressDialog = new ProgressDialog(mContext);
        firebaseAuth = FirebaseAuth.getInstance();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!etPass.getText().toString().equalsIgnoreCase(etConfPass.getText().toString())){
                    Toast.makeText(mContext, "Password tidak sesuai", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }else if (spAge.getSelectedItemPosition() == 0){
                    Toast.makeText(mContext, "Harap Pilih Gender", Toast.LENGTH_SHORT).show();
                } else if (
                        etNama.getText().toString().equals("") ||
                        etUsername.getText().toString().equals("") ||
                        etEmail.getText().toString().equals("") ||
                        etPass.getText().toString().equals("") ||
                        etConfPass.getText().toString().equals("") ||
                        etAddress.getText().toString().equals("") ||
                        etAge.getText().toString().equals(""))
//                                || etGender.getText().toString().equals(""))
                {

                    Toast.makeText(mContext, "Kolom Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                } else {
                    progressDialog = ProgressDialog.show(mContext, null, "loading", true, false);
                    requestRegister();
                }
            }
        });
    }

    private void requestRegister() {

        String gen = "";

        if (spAge.getSelectedItemPosition() == 1){
            gen = "M";
        }else if (spAge.getSelectedItemPosition() == 2){
            gen = "F";
        }

        Call<ResponseBody> postRegister = apiService.postRegister(
                etNama.getText().toString(),
                etUsername.getText().toString(),
                etEmail.getText().toString(),
                etPass.getText().toString(),
                etAddress.getText().toString(),
                Integer.parseInt(etAge.getText().toString()),
//                etGender.getText().toString(),
                gen,
                prefshelper.getGToken(),
                "occLe6UWGtOOMMg0OLvAMI3D45uOjzsUQql1i7qhsW0=");

        postRegister.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();

                    try {
                        JSONObject jsonResult = new JSONObject(response.body().string());

                        if (jsonResult.getString("error").equals("false")){

                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                    Toast.makeText(mContext, "Registrasi Berhasil", Toast.LENGTH_SHORT).show();
                    firebaseAuth.createUserWithEmailAndPassword(etEmail.getText().toString(), etPass.getText().toString());
                    finish();
                }else {
                    Log.i("debug", "onResponse : "+response.message());
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("debug", "onFailure : Error " + t.getMessage());
                Toast.makeText(mContext, "Registrasi Gagal", Toast.LENGTH_SHORT).show();
            }
        });
    }


    // setting for min max age
    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            int input = Integer.parseInt(dest.toString() + source.toString());
            if (isInrange(min, max, input))
                return null;
        }catch (NumberFormatException e){

        }
        return "";
    }

    private boolean isInrange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}
