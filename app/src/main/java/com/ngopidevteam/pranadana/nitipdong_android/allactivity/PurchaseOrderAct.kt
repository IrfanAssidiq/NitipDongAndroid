package com.ngopidevteam.pranadana.nitipdong_android.allactivity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ngopidevteam.pranadana.nitipdong_android.R
import com.ngopidevteam.pranadana.nitipdong_android.utils.SharedPrefManager
import kotlinx.android.synthetic.main.activity_pembayaran.*
import kotlinx.android.synthetic.main.activity_pembayaran.bayar
import kotlinx.android.synthetic.main.appbar_gradient.*


/**
 *   created by Irfan Assidiq on 2020-02-20
 *   email : assidiq.irfan@gmail.com
 **/
class PurchaseOrderAct : AppCompatActivity() {
    lateinit var sp : SharedPrefManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pembayaran)
        val window = window
        window.statusBarColor = resources.getColor(android.R.color.transparent)
        window.navigationBarColor = resources.getColor(R.color.orange)
        window.setBackgroundDrawable(resources.getDrawable(R.drawable.btn_purple))

        sp = SharedPrefManager(this)

        bayar.setOnClickListener {
            startActivity(Intent(this@PurchaseOrderAct, Homepeg::class.java))
            finish()
        }
        btn_back.setOnClickListener {
            onBackPressed()
        }
    }
}