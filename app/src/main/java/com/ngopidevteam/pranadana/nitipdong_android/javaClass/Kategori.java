package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

public class Kategori {
    private int gambarKategori;
    private String textOke;

    public Kategori(){

    }

    public Kategori(int gambarKategori, String textOke) {
        this.gambarKategori = gambarKategori;
        this.textOke = textOke;
    }

    public int getGambarKategori() {
        return gambarKategori;
    }

    public void setGambarKategori(int gambarKategori) {
        this.gambarKategori = gambarKategori;
    }

    public String getTextOke(){
        return this.textOke;
    }
}
