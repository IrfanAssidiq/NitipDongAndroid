package com.ngopidevteam.pranadana.nitipdong_android.allactivity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.ngopidevteam.pranadana.nitipdong_android.R
import kotlinx.android.synthetic.main.activity_purchase.*


/**
 *   created by Irfan Assidiq on 2020-02-18
 *   email : assidiq.irfan@gmail.com
 **/
class DetailPurchaseAct : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_purchase)
        val window = window
        window.statusBarColor = resources.getColor(R.color.putih)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }

        bayar.setOnClickListener {
            startActivity(Intent(this, PurchaseOrderAct::class.java))
            finish()
        }
        nitipLagi.setOnClickListener {
            onBackPressed()
        }
    }
}