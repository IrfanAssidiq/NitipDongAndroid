package com.ngopidevteam.pranadana.nitipdong_android.allactivity

import android.os.Bundle
import android.util.Log.e
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ngopidevteam.pranadana.nitipdong_android.R
import com.ngopidevteam.pranadana.nitipdong_android.adapter.KategoriHorizontalAdapter
import com.ngopidevteam.pranadana.nitipdong_android.adapter.NegaraHorizontalAdapter
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Kategori
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Negara
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.NegaraOKE
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.NegaraResponse
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIClient
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIService
import kotlinx.android.synthetic.main.alldetailiitem.*
import kotlinx.android.synthetic.main.appbar_gradient.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 *   created by Irfan Assidiq on 2020-02-21
 *   email : assidiq.irfan@gmail.com
 **/
class AllListdata : AppCompatActivity() {

    var kategoriList = ArrayList<Kategori>()
    var negaraList = ArrayList<NegaraOKE>()
    var data:String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alldetailiitem)
        rcView.setHasFixedSize(true)
        rcView.layoutManager = GridLayoutManager(this, 3)

        data = intent.getStringExtra("data")

        if (data.equals("kategori")){
            titlebar.text = "Semua Kategori"
            populateKategoriList()
            val kategoriAdapter = KategoriHorizontalAdapter(kategoriList, this)
            rcView.adapter = kategoriAdapter
        }else if (data.equals("negara")){
            titlebar.text = "Semua Negara"
            populateNegaraList()
        }

        btn_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun populateNegaraList() {
        var negaraOke : NegaraOKE
        APIClient.getClient().create(APIService::class.java).listCountry(resources.getString(R.string.apikey))
            .enqueue(object : Callback<NegaraResponse>{
                override fun onFailure(call: Call<NegaraResponse>?, t: Throwable?) {
                    Toast.makeText(this@AllListdata, ""+t.toString(), Toast.LENGTH_SHORT).show()
                    e("TAGERRROR", t!!.message)
                }

                override fun onResponse(
                    call: Call<NegaraResponse>?,
                    response: Response<NegaraResponse>?
                ) {
                    if (response != null) {
                        if (response.code() == 200){
                            val databody = response.body()
                            for(x in 0..10 ){ //add databody.data!!.indices
                                negaraOke = NegaraOKE(databody.data!![x].country_id,
                                                        databody.data!![x].code,
                                                        databody.data!![x].name)
                                negaraList.add(negaraOke)
                                e("TAGOKE",negaraOke.name)
                            }
                            val negaraAdapter = NegaraHorizontalAdapter(negaraList, this@AllListdata)
                            rcView.adapter = negaraAdapter

                            Toast.makeText(this@AllListdata, "Oke Nich", Toast.LENGTH_SHORT).show()
                        }else{
                            Toast.makeText(this@AllListdata, "Gagal", Toast.LENGTH_SHORT).show()
                        }
                    }
                }

            })

    }

    private fun populateKategoriList() {
        val satu = Kategori(R.drawable.tas_pertama, "TAS")
        val dua = Kategori(R.drawable.jamtangan, "Jam Tangan")
        val tiga = Kategori(R.drawable.sepatu, "Sepatu")
        val empat = Kategori(R.drawable.gadget, "Gadget")
        val lima = Kategori(R.drawable.kaos, "Kaos")

        kategoriList.add(satu)
        kategoriList.add(dua)
        kategoriList.add(tiga)
        kategoriList.add(empat)
        kategoriList.add(lima)
    }
}