package com.ngopidevteam.pranadana.nitipdong_android.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ngopidevteam.pranadana.nitipdong_android.allactivity.InfoAkunPribadi;
import com.ngopidevteam.pranadana.nitipdong_android.allactivity.Loggin;
import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.allactivity.Riwayat;
import com.ngopidevteam.pranadana.nitipdong_android.fragment.DetailAkunFragment;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.DetailProfil;
import com.ngopidevteam.pranadana.nitipdong_android.utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

public class ProfilAdapter extends RecyclerView.Adapter<ProfilAdapter.DetailViewHolder> {
    private List<DetailProfil> detailProfileList = new ArrayList<>();

    private Context context;

    public ProfilAdapter(Context ctx){
        this.context = ctx;
    }
    public ProfilAdapter(){

    }

    @NonNull
    @Override
    public DetailViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_detail_profile, viewGroup, false);


        return new DetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DetailViewHolder detailViewHolder, final int i) {
        final DetailProfil detailProfil = detailProfileList.get(i);
        final SharedPrefManager sharedPrefManager = new SharedPrefManager(context);

        detailViewHolder.txtDetailProduk.setText(detailProfil.getTxtDetailProfile());

        detailViewHolder.txtDetailProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DetailAkunFragment detailAkunFragment = new DetailAkunFragment();
                Intent intent = new Intent();


                switch (i){
                    case 0:
                        intent = new Intent(context, InfoAkunPribadi.class);
                        context.startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(context, Riwayat.class);
                        context.startActivity(intent);
                        break;
                    case 2:
                        Toast.makeText(context, "Fitur ini belum tersedia", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        Toast.makeText(context, "Fitur ini belum tersedia", Toast.LENGTH_SHORT).show();
                        break;
                    case 4:
                        Toast.makeText(context, "Fitur ini belum tersedia", Toast.LENGTH_SHORT).show();
                        break;
                    case 5:
                        Toast.makeText(context, "Fitur ini belum tersedia", Toast.LENGTH_SHORT).show();
                        break;
                    case 6:
                        sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
                        sharedPrefManager.saveSPString(sharedPrefManager.SP_TOKEN, "");
                        intent = new Intent(context, Loggin.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return detailProfileList.size();
    }

    public void updateData(List<DetailProfil> detailList){
        this.detailProfileList = detailList;
        notifyDataSetChanged();
    }


    public class DetailViewHolder extends RecyclerView.ViewHolder{
        private TextView txtDetailProduk;

        public DetailViewHolder(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            txtDetailProduk = itemView.findViewById(R.id.txt_detail_produk);

        }
    }


}
