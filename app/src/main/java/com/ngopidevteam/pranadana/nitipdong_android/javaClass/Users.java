package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Users {

    @Expose
    @SerializedName("username") String username;

    @Expose
    @SerializedName("name") String name;

    @Expose
    @SerializedName("email") String email;

    @Expose
    @SerializedName("address") String address;

    @Expose
    @SerializedName("age") String age;

    @Expose
    @SerializedName("gender") String gender;

    @Expose
    @SerializedName("code") String code;

    @Expose
    @SerializedName("merchant_id") String merchant_id;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }
}
