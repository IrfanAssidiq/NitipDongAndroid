package com.ngopidevteam.pranadana.nitipdong_android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.allactivity.ProdukNegara;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Negara;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.NegaraOKE;

import java.util.ArrayList;
import java.util.List;

public class NegaraHorizontalAdapter extends RecyclerView.Adapter<NegaraHorizontalAdapter.ViewHolder> {

    private ArrayList<NegaraOKE> horizontalNegara;
    Context context;
    NegaraOKE negara;

    public NegaraHorizontalAdapter(ArrayList<NegaraOKE> horizontalNegara, Context context) {
        this.horizontalNegara = horizontalNegara;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_negara, parent, false);
        ViewHolder v = new ViewHolder(view);
        return v;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        negara = horizontalNegara.get(position);
        Glide.with(context)
                .load(R.drawable.belanda)
                .error(R.drawable.belanda)
                .placeholder(R.mipmap.ic_nitipdong)
                .into(holder.imgNegara);

        String namaNegara = negara.getName();
        Toast.makeText(context, ""+namaNegara, Toast.LENGTH_SHORT).show();
        holder.tvNegara.setText(negara.getName());
        holder.imgNegara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String namaNegara = horizontalNegara.get(position).getName();

                Toast.makeText(context, ""+namaNegara, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, ProdukNegara.class);
                intent.putExtra("nama", namaNegara);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return horizontalNegara.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgNegara;
        TextView tvNegara;

        ViewHolder(View itemView) {
            super(itemView);
            imgNegara = itemView.findViewById(R.id.img_negara);
            tvNegara = itemView.findViewById(R.id.tv_negara);
        }
    }
}
