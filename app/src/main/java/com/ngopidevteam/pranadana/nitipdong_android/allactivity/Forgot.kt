package com.ngopidevteam.pranadana.nitipdong_android.allactivity

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.ngopidevteam.pranadana.nitipdong_android.R
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIClient
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIService
import kotlinx.android.synthetic.main.activity_forgot.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Forgot : AppCompatActivity() {

    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot)

        var subject:String = "forgot_password"
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading")

        btn_send.setOnClickListener {
            progressDialog.show()
            APIClient.getClient().create(APIService::class.java).forgotPassword(resources.getString(R.string.apikey), txtEmail.text.toString(), subject)
                .enqueue(object : Callback<ResponseBody>{
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        progressDialog.dismiss()
                        Toast.makeText(applicationContext, "Tidak dapat terkoneksi", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(
                        call: Call<ResponseBody>?,
                        response: Response<ResponseBody>?
                    ) {
                        if (response != null) {
                            if (response.code() == 200){
                                progressDialog.dismiss()
                                Toast.makeText(applicationContext, "Success", Toast.LENGTH_SHORT).show()
                                finish()
                            }else{
                                progressDialog.dismiss()
                                Toast.makeText(applicationContext, "Email tidak valid", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                })
        }
    }
}
