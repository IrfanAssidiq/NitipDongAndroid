package com.ngopidevteam.pranadana.nitipdong_android.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Users;

import java.util.List;

public class UserResponse extends BaseResponse {

    //login
    @Expose
    @SerializedName("token") String token;

    @Expose
    @SerializedName("expired") String expired;

    @Expose
    @SerializedName("refresh") String refresh;


    //register
    @Expose
    @SerializedName("name") String name;

    @Expose
    @SerializedName("username") String username;

    @Expose
    @SerializedName("email") String email;

    @Expose
    @SerializedName("password") String password;

    @Expose
    @SerializedName("gender") String gender;

    @Expose
    @SerializedName("age") int age;

    @Expose
    @SerializedName("address") String address;

    @Expose
    @SerializedName("apikey") String apikey;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getApikey() {
        return apikey;
    }
//
    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

//    public Users getUser() {
//        return user;
//    }
//
//    public void setUser(Users user) {
//        this.user = user;
//    }
}
