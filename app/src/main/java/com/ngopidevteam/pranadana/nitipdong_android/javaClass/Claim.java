package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

import androidx.annotation.Nullable;

import java.util.Date;
import java.util.List;

public interface Claim {

    @Nullable
    Boolean asBoolean();

    @Nullable
    Integer asInt();

    @Nullable
    Long asLong();

    @Nullable
    Double asDouble();

    @Nullable
    String asString();

    @Nullable
    Date asDate();

    <T> T[] asArray(Class<T> tClass) throws DecodeException;

    <T> List<T> asList(Class<T> tClass) throws DecodeException;

    @Nullable
    <T> T asObject(Class<T> tClass) throws DecodeException;
}
