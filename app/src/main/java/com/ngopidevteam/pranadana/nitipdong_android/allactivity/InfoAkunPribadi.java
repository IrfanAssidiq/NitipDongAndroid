package com.ngopidevteam.pranadana.nitipdong_android.allactivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.auth0.android.jwt.JWT;
import com.bumptech.glide.Glide;
import com.ngopidevteam.pranadana.nitipdong_android.BuildConfig;
import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Users;
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIClient;
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIService;
import com.ngopidevteam.pranadana.nitipdong_android.utils.SharedPrefManager;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoAkunPribadi extends AppCompatActivity {

    int pickPhoto;
    private String cameraFilePath;

    SharedPrefManager sharedPrefManager;
    ProgressDialog progressDialog;
    APIService apiService;

    EditText etUsername, etName, etEmail, etAddress, etAge;
    Spinner spGender;
    Button btnConfirm;

    ImageView imageView;
    int TAKE_PHOTO_CODE = 0;
    public static int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_akun_pribadi);

        etUsername = findViewById(R.id.profil_username);
        etName = findViewById(R.id.profil_name);
        etEmail = findViewById(R.id.profil_email);
        etAddress = findViewById(R.id.profil_address);
        etAge = findViewById(R.id.profil_age);
        spGender = findViewById(R.id.profil_gender);
        btnConfirm = findViewById(R.id.profil_confirm);
        imageView = findViewById(R.id.imageProfile);

        apiService = APIClient.getClient().create(APIService.class);
        sharedPrefManager = new SharedPrefManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        progressDialog.setCancelable(false);

        JWT jwt = new JWT(sharedPrefManager.getSPToken());

        Users users =  jwt.getClaim("usr").asObject(Users.class);

        etUsername.setText(users.getName());
        etName.setText(users.getName());
        etEmail.setText(users.getEmail());
        etAddress.setText(users.getAddress());
        etAge.setText(users.getAge());
        if (users.getGender().equals("M")) {
            spGender.setSelection(1);
            Toast.makeText(this, "M", Toast.LENGTH_SHORT).show();
        }else if (users.getGender().equals("F")){
            spGender.setSelection(2);
            Toast.makeText(this, "Female", Toast.LENGTH_SHORT).show();
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                String gen = "";

                if (spGender.getSelectedItemPosition() == 1) {
                    gen = "M";
                } else if (spGender.getSelectedItemPosition() == 2) {
                    gen = "F";
                }

                    Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();

                    String encode = encodeToBase64(bitmap);

                    insertData(getResources().getString(
                            R.string.apikey),
                            etUsername.getText().toString(),
                            etName.getText().toString(),
                            etAddress.getText().toString(),
                            Integer.parseInt(etAge.getText().toString()),
                            gen,
                            "data:image/jpeg;base64," + encode);
            }
        });
    }

    private void insertData(String apikey, String username, String name, String address, int age, String gen, String encode) {
        Call<ResponseBody> postUpdateProfil = apiService.postUpdateProfil(
                apikey, username, name, address, age, gen, encode
        );

        postUpdateProfil.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    Toast.makeText(InfoAkunPribadi.this, "Success", Toast.LENGTH_SHORT).show();
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(InfoAkunPribadi.this, response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(InfoAkunPribadi.this, "gagal "+ t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String encodeToBase64(Bitmap bitmap) {
        Bitmap imgEncode = bitmap;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imgEncode.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] b = baos.toByteArray();
        String imgEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        return imgEncoded;
    }

    public void pickImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(InfoAkunPribadi.this);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (options[which].equals("Take Photo")){
                    try{
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(
                                InfoAkunPribadi.this, BuildConfig.APPLICATION_ID + ".provider", createImageFile()));
                        startActivityForResult(intent, 1);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (options[which].equals("Choose from Gallery")){
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    startActivityForResult(intent, 2);
                }else if (options[which].equals("Cancel")){
                    dialog.dismiss();
                }
            }
        });
        builder.show();

//            imageView = findViewById(R.id.imageProfile);
//            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//            intent.setType("image/*");

//            startActivityForResult(intent, pickPhoto);
        }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_"+timeStamp+"_";

        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(imageFileName, ".jpeg", storageDir);

        cameraFilePath = "file://"+image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == 1){
                imageView.setImageURI(Uri.parse(cameraFilePath));
            }else if (requestCode == 2){
                if (data == null){
                    return;
                }
                try {
                    Uri selectedImageUri = data.getData();

                    final String path = getPathFromUri(selectedImageUri);
                    if (path != null) {
                        File f = new File(path);
                        selectedImageUri = Uri.fromFile(f);
                    }

                    Glide.with(InfoAkunPribadi.this)
                            .load(selectedImageUri)
                            .error(R.drawable.ic_launcher_background)
                            .into(imageView);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }
    }

    private String getPathFromUri(Uri selectedImageUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImageUri, proj, null, null, null);

        if (cursor.moveToFirst()){
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res= cursor.getString(column_index);
        }

        cursor.close();
        return res;
    }
}