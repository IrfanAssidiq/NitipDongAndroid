package com.ngopidevteam.pranadana.nitipdong_android.adapter;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.ProdukNegaraModel;

import java.util.List;

public class ProdukNegaraAdapter extends RecyclerView.Adapter<ProdukNegaraAdapter.ViewHolder> {

    private List<ProdukNegaraModel> produkList;

    public ProdukNegaraAdapter(List<ProdukNegaraModel> produkList) {
        this.produkList = produkList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_produk_negara, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProdukNegaraModel produkNegaraModel = produkList.get(position);
        final int pos = position;

        holder.imgProdukNegara.setImageResource(produkNegaraModel.getImgProdukNegara());
        holder.namaProdukNegara.setText(produkNegaraModel.getNamaProdukNegara());
        holder.hargaProdukNegara.setText(produkNegaraModel.getHargaProdukNegara());
    }

    @Override
    public int getItemCount() {
        return produkList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgProdukNegara;
        private TextView namaProdukNegara, hargaProdukNegara;

        public ViewHolder(View itemView) {
            super(itemView);

            imgProdukNegara = itemView.findViewById(R.id.img_produk_negara);
            namaProdukNegara = itemView.findViewById(R.id.nama_produk_negara);
            hargaProdukNegara = itemView.findViewById(R.id.hrg_produk_negara);
        }
    }
}
