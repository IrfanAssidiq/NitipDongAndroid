package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class ClaimImpl extends BaseClaim {

    private final JsonElement value;

    ClaimImpl(@NonNull JsonElement value){
        this.value = value;
    }

    @Override
    @Nullable
    public Boolean asBoolean(){
        if (!value.isJsonPrimitive()){
            return null;
        }
        return value.getAsBoolean();
    }

    @Override
    @Nullable
    public Integer asInt(){
        if (!value.isJsonPrimitive()){
            return null;
        }
        return value.getAsInt();
    }

    @Override
    @Nullable
    public Long asLong(){
        if (!value.isJsonPrimitive()){
            return null;
        }
        return value.getAsLong();
    }

    @Override
    @Nullable
    public Double asDouble(){
        if (!value.isJsonPrimitive()){
            return null;
        }
        return value.getAsDouble();
    }

    @Override
    @Nullable
    public String asString(){
        if (!value.isJsonPrimitive()){
            return null;
        }
        return value.getAsString();
    }

    @Override
    @Nullable
    public Date asDate(){
        if (!value.isJsonPrimitive()){
            return null;
        }
        long ms = Long.parseLong(value.getAsString()) * 1000;
        return new Date(ms);
    }

    @Override
    public <T> T[] asArray(Class<T> tClass) throws DecodeException {
        try {
            if (!value.isJsonArray() || value.isJsonNull()){
                return (T[]) Array.newInstance(tClass, 0);
            }

            Gson gson = new Gson();
            JsonArray jsonArray = value.getAsJsonArray();
            T[] arr = (T[]) Array.newInstance(tClass, jsonArray.size());
            for (int i = 0; i < jsonArray.size(); i++){
                arr[i] = gson.fromJson(jsonArray.get(i), tClass);
            }
            return arr;
        }catch (JsonSyntaxException e){
            throw new DecodeException("Failed to decode claim as array", e);
        }
    }

    @Override
    public <T> List<T> asList(Class<T> tClass) throws DecodeException {
        try {
            if (!value.isJsonArray() || value.isJsonNull()){
                return new ArrayList<>();
            }

            Gson gson = new Gson();
            JsonArray jsonArray = value.getAsJsonArray();
            List<T> list = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++){
                list.add(gson.fromJson(jsonArray.get(i), tClass));
            }
            return list;
        }catch (JsonSyntaxException e){
            throw new DecodeException("Failed to decode claim as list", e);
        }
    }


    @Override
    public <T> T asObject(Class<T> tClass) throws DecodeException {
        try {
            if (value.isJsonNull()){
                return null;
            }
            return new Gson().fromJson(value, tClass);
        }catch (JsonSyntaxException e){
            throw new DecodeException("Failed to decode claim as " + tClass.getSimpleName(), e);
        }
    }


}
