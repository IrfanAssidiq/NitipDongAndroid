package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JWTDeserializer implements JsonDeserializer<JWTPayload> {
    @Override
    public JWTPayload deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (json.isJsonNull() || !json.isJsonObject()){
            throw new DecodeException("The token's payload had an invalid JSON format");
        }

        JsonObject object = json.getAsJsonObject();

        String iss = getString(object, "iss");
        String sub = getString(object, "sub");
        Date exp = getDate(object, "exp");
        Date nbf = getDate(object, "nbf");
        Date iat = getDate(object, "iat");
        String jti = getString(object, "jti");
        List<String> aud = getStringOrArray(object, "aud");
        List<String> usr = getStringOrArray(object, "usr");

        Map<String, Claim> extra = new HashMap<>();
        for (Map.Entry<String, JsonElement> e : object.entrySet()){
            extra.put(e.getKey(), new ClaimImpl(e.getValue()));
        }

        return new JWTPayload(iss, sub, exp, nbf, iat, jti, aud, usr, extra);
    }

    private Date getDate(JsonObject object, String claimName) {

        if (!object.has(claimName)){
            return null;
        }
        long ms = object.get(claimName).getAsLong() * 1000;
        return new Date(ms);
    }

    private String getString(JsonObject object, String claimName) {
        if (!object.has(claimName)){
            return null;
        }
        return object.get(claimName).getAsString();
    }

    public List<String> getStringOrArray(JsonObject object, String claimName) {
        List<String> list = Collections.emptyList();
        if (object.has(claimName)){
            JsonElement arrElement = object.get(claimName);
            if (arrElement.isJsonArray()){
                JsonArray jsonArray = arrElement.getAsJsonArray();
                list = new ArrayList<>(jsonArray.size());
                for (int i = 0; i <jsonArray.size(); i++){
                    list.add(jsonArray.get(i).getAsString());
                }
            } else {
                list = Collections.singletonList(arrElement.getAsString());
            }
        }
        return list;
    }
}