package com.ngopidevteam.pranadana.nitipdong_android.allactivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.adapter.TrendingAdapter;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.TrendingJastipModel;

import java.util.ArrayList;
import java.util.List;

public class TrendingJastip extends AppCompatActivity {

    private RecyclerView recTrending;
    private RecyclerView.Adapter trendingAdapter;
    private List<TrendingJastipModel> trendingJastipModelList;
    private ImageView btn_back_trending;
    private TextView tvNotFound, titlebar;
    private LinearLayout llayout;
    public TrendingJastip() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending_jastip);

        Window window = getWindow();
        recTrending = findViewById(R.id.rec_trending);
        btn_back_trending = findViewById(R.id.btn_back_trending);
        tvNotFound = findViewById(R.id.tvNotFound);
        titlebar = findViewById(R.id.titlebar);
        llayout = findViewById(R.id.llayout);
        recTrending.setHasFixedSize(true);
        String data = getIntent().getStringExtra("kategori");
        if(data.equals("TAS") || data.equals("CEKSEMUA")){
            tvNotFound.setVisibility(View.GONE);
            recTrending.setVisibility(View.VISIBLE);
            if(data.equals("TAS")){
                titlebar.setText("Kategori : "+data);
                window.setNavigationBarColor(getResources().getColor(R.color.orange));
                window.setStatusBarColor(getResources().getColor(R.color.orange));
                llayout.setBackground(getResources().getDrawable(R.drawable.btn_orange_btmradius));
            }else{
                titlebar.setText("Jastip Terkini");
                window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
                window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
                window.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_purple));
            }
        }else{
            titlebar.setText("Kategori : "+data);
            tvNotFound.setVisibility(View.VISIBLE);
            recTrending.setVisibility(View.GONE);
            tvNotFound.setText("Belum ada data pada kategori ini");
            window.setNavigationBarColor(getResources().getColor(R.color.orange));
            window.setStatusBarColor(getResources().getColor(R.color.orange));
            llayout.setBackground(getResources().getDrawable(R.drawable.btn_orange_btmradius));

        }

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recTrending.setLayoutManager(gridLayoutManager);
//        window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//        window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//        window.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_purple));

        trendingJastipModelList = new ArrayList<>();
        trendingJastipModelList.add(new TrendingJastipModel(R.drawable.tas_pertama, "Mandala Bag", "500000"));
        trendingJastipModelList.add(new TrendingJastipModel(R.drawable.tas_kedua, "Gucci Bag", "1000000"));
        trendingJastipModelList.add(new TrendingJastipModel(R.drawable.tas_ketiga, "Bruno Bag", "750000"));
        trendingJastipModelList.add(new TrendingJastipModel(R.drawable.tas_keempat, "Alea Bag", "5000000"));
        trendingJastipModelList.add(new TrendingJastipModel(R.drawable.tas_kelima, "Karina Bag", "6000000"));

        trendingAdapter = new TrendingAdapter(trendingJastipModelList, this);
        recTrending.setAdapter(trendingAdapter);

        btn_back_trending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
