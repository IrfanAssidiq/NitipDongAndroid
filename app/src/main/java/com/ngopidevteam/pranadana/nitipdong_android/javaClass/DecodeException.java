package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

public class DecodeException extends RuntimeException {

    DecodeException(String message){
        super(message);
    }

    DecodeException(String message, Throwable cause){
        super(message, cause);
    }
}
