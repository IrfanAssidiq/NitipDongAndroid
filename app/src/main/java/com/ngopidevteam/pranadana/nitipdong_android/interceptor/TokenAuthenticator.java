package com.ngopidevteam.pranadana.nitipdong_android.interceptor;

import android.content.Intent;

import com.ngopidevteam.pranadana.nitipdong_android.allactivity.Loggin;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.NitipDongAndroid;
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIClient;
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIService;
import com.ngopidevteam.pranadana.nitipdong_android.utils.SharedPrefManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenAuthenticator implements Interceptor {

    SharedPrefManager sharedPrefManager;

    public TokenAuthenticator() {
        sharedPrefManager = new SharedPrefManager(NitipDongAndroid.getContext());
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response mainResponse = chain.proceed(chain.request());
        Request mainRequest = chain.request();
        APIService apiService = APIClient.getClient().create(APIService.class);

        if (mainResponse.code() == 401 || mainResponse.code() == 403){
//            String token = sharedPrefManager.getSPToken();
            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
            Intent i = new Intent(NitipDongAndroid.getContext(), Loggin.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            NitipDongAndroid.getContext().startActivity(i);
        }else if (mainResponse.code() == 500){
            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
            Intent i = new Intent(NitipDongAndroid.getContext(), Loggin.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            NitipDongAndroid.getContext().startActivity(i);
        }

        return mainResponse;
    }
}
