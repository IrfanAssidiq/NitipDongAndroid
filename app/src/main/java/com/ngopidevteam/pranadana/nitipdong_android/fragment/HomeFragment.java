package com.ngopidevteam.pranadana.nitipdong_android.fragment;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.adapter.JastipHorizontalAdapter;
import com.ngopidevteam.pranadana.nitipdong_android.adapter.KategoriHorizontalAdapter;
import com.ngopidevteam.pranadana.nitipdong_android.adapter.NegaraHorizontalAdapter;
import com.ngopidevteam.pranadana.nitipdong_android.allactivity.AllListdata;
import com.ngopidevteam.pranadana.nitipdong_android.allactivity.TrendingJastip;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Jastip;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Kategori;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Negara;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.NegaraOKE;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.NegaraResponse;
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIClient;
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIService;
import com.ngopidevteam.pranadana.nitipdong_android.utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.util.Log.e;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    private List<Jastip> jastipList = new ArrayList<>();
    private List<Kategori> kategoriList = new ArrayList<>();
    private ArrayList<NegaraOKE> negaraList = new ArrayList<>();
    private RecyclerView jastipRecyclerView, kategoriRecyclerView, negaraRecyclerView;
    private JastipHorizontalAdapter produkAdapter;
    private KategoriHorizontalAdapter kategoriAdapter;
    private NegaraHorizontalAdapter negaraAdapter;
    Toolbar toolbar;
    ImageView bell, love, mail;
    SharedPrefManager spmanager;
    TextView username, cekSemua, cek_kategori, cek_negara;
    NegaraOKE negaraOKE ;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        jastipRecyclerView = view.findViewById(R.id.rec_jastip_horizontal);
        jastipRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.HORIZONTAL));
        produkAdapter = new JastipHorizontalAdapter(jastipList, getContext());

        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        jastipRecyclerView.setLayoutManager(llm);
        jastipRecyclerView.setAdapter(produkAdapter);
        populateProdukList();

        kategoriRecyclerView = view.findViewById(R.id.rec_kategori_horizontal);
        kategoriRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.HORIZONTAL));
        kategoriAdapter = new KategoriHorizontalAdapter(kategoriList, getContext());

        LinearLayoutManager llmm = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        kategoriRecyclerView.setLayoutManager(llmm);
        kategoriRecyclerView.setAdapter(kategoriAdapter);
        populateKategoriList();

        negaraRecyclerView = view.findViewById(R.id.rec_negara_horizontal);
//        negaraRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.HORIZONTAL));
        LinearLayoutManager llmmm = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        negaraRecyclerView.setLayoutManager(llmmm);
        negaraRecyclerView.setHasFixedSize(true);

        toolbar = view.findViewById(R.id.toolbar);
//        Toolbar toolbar = view.findViewById(R.id.toolbar_homepage);

        cekSemua = view.findViewById(R.id.cek_jastip);
        cekSemua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), TrendingJastip.class);
                intent.putExtra("kategori", "CEKSEMUA");
                startActivity(intent);
            }
        });
        cek_kategori = view.findViewById(R.id.cek_kategori);
        cek_kategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AllListdata.class);
                intent.putExtra("data", "kategori");
                startActivity(intent);
            }
        });

        cek_negara = view.findViewById(R.id.cek_negara);
        cek_negara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AllListdata.class);
                intent.putExtra("data", "negara");
                startActivity(intent);
            }
        });
        return view;
    }

    private void populateNegaraList() {
         APIClient.getClient().create(APIService.class).listCountry(getResources().getString(R.string.apikey))
                .enqueue(new Callback<NegaraResponse>() {
                    @Override
                    public void onResponse(Call<NegaraResponse> call, Response<NegaraResponse> response) {
                        if (response != null) {
                            if (response.code() == 200){
                                NegaraResponse databody = response.body();
                                for(int x = 0; x < 5; x++){
                                    negaraOKE = new NegaraOKE(databody.getData().get(x).getCountry_id(),
                                            databody.getData().get(x).getCode(), databody.getData().get(x).getName());

                                    negaraList.add(negaraOKE);
                                }
                                negaraAdapter = new NegaraHorizontalAdapter(negaraList, getContext());
                                negaraRecyclerView.setAdapter(negaraAdapter);
                                e("TAGOKE",databody.getData().get(1).getName());
                                Toast.makeText(getContext(), "Oke Nich", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getContext(), "Gagal", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<NegaraResponse> call, Throwable t) {
                        Toast.makeText(getContext(), "Gagal", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void populateKategoriList() {
        Kategori satu = new Kategori(R.drawable.tas_pertama, "TAS");
        Kategori dua = new Kategori(R.drawable.jamtangan, "Jam Tangan");
        Kategori tiga = new Kategori(R.drawable.sepatu, "Sepatu");
        Kategori empat = new Kategori(R.drawable.gadget, "Gadget");
        Kategori lima = new Kategori(R.drawable.kaos, "Kaos");

        kategoriList.add(satu);
        kategoriList.add(dua);
        kategoriList.add(tiga);
        kategoriList.add(empat);
        kategoriList.add(lima);
    }

    private void populateProdukList() {
        Jastip pertama = new Jastip( R.drawable.tas_pertama, "Mandala Bag","500000");
        Jastip kedua = new Jastip( R.drawable.tas_kedua,"Gucci Bag", "1000000");
        Jastip ketiga = new Jastip(R.drawable.tas_ketiga, "Bruno bag", "750000");
        Jastip keempat = new Jastip( R.drawable.tas_keempat, "Alea bag","5000000");
        Jastip kelima = new Jastip( R.drawable.tas_kelima, "Karina bag","6000000");

        jastipList.add(pertama);
        jastipList.add(kedua);
        jastipList.add(ketiga);
        jastipList.add(keempat);
        jastipList.add(kelima);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        populateNegaraList();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bell = toolbar.findViewById(R.id.bell);
        love = toolbar.findViewById(R.id.love);
        mail = toolbar.findViewById(R.id.message);
        username = toolbar.findViewById(R.id.username);

        Window window = getActivity().getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.biru));
        window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_background_gradient));
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        }
        bell.setOnClickListener(this);
        love.setOnClickListener(this);
        mail.setOnClickListener(this);
        spmanager = new SharedPrefManager(getActivity().getApplicationContext());
        String textoke = spmanager.getSpUsername();
        username.setText(textoke);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.love :{
                Toast.makeText(getActivity().getApplicationContext(), "fitur dalam pengembangan", Toast.LENGTH_SHORT).show();
            }break;
            case R.id.bell :{
                Toast.makeText(getActivity().getApplicationContext(), "fitur dalam pengembangan", Toast.LENGTH_SHORT).show();
            }break;
            case R.id.message :{
                Toast.makeText(getActivity().getApplicationContext(), "fitur dalam pengembangan", Toast.LENGTH_SHORT).show();
            }break;
            default:
                //nothing to do
        }
    }
}
