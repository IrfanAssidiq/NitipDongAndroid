package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

import android.app.Application;
import android.content.Context;

public class NitipDongAndroid extends Application {

    private static NitipDongAndroid instance;

    public static NitipDongAndroid getInstance(){
        return instance;
    }

    public static Context getContext(){
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
    }
}
