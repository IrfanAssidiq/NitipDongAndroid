package com.ngopidevteam.pranadana.nitipdong_android.allactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.auth0.android.jwt.JWT;
import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIService;
import com.ngopidevteam.pranadana.nitipdong_android.remote.APIClient;
import com.ngopidevteam.pranadana.nitipdong_android.response.UserResponse;
import com.ngopidevteam.pranadana.nitipdong_android.utils.SharedPrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Loggin extends AppCompatActivity {

    private EditText etEmail, etPass;
    private Button btnLogin;
    TextView forgot, creates;

    Context context;
    ProgressDialog progressDialog;
    SharedPrefManager sharedPrefManager;
    APIService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Window window = getWindow();

        etEmail = findViewById(R.id.et_email);
        etPass = findViewById(R.id.et_pass);
        btnLogin = findViewById(R.id.btn_login);
        forgot = findViewById(R.id.forgot);
        creates = findViewById(R.id.create);

        context = this;
        apiService = APIClient.getClient().create(APIService.class);
        sharedPrefManager = new SharedPrefManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        progressDialog.setCancelable(false);

        if (sharedPrefManager.getSPSudahLogin()){
            startActivity(new Intent(Loggin.this, Homepeg.class)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Loggin.this, Forgot.class));
            }
        });
        creates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Loggin.this, Register.class));
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                Call<UserResponse> postLogin = apiService.postLogin(etEmail.getText().toString(), etPass.getText().toString(), getResources().getString(R.string.apikey));
                postLogin.enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                        UserResponse userResponses = response.body();

                        if (response.code() == 200){
                            sharedPrefManager.saveSPString(SharedPrefManager.SP_TOKEN, ""+ userResponses.getToken());
                            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);
                            sharedPrefManager.setSpName(etEmail.getText().toString());

                            JWT jwt = new JWT(sharedPrefManager.getSPToken());
                            Toast.makeText(context, ""+jwt.getExpiresAt(), Toast.LENGTH_SHORT).show();
                            

                            startActivity(new Intent(context, Homepeg.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            progressDialog.dismiss();
                            finish();

                        }else{
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        t.printStackTrace();
                    }
                });
            }
        });
    }
}
