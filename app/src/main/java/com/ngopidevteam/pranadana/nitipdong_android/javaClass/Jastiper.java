package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

public class Jastiper {
    int jastiperImage;
    String nama, harga;

    public int getJastiperImage() {
        return jastiperImage;
    }

    public void setJastiperImage(int jastiperImage) {
        this.jastiperImage = jastiperImage;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public Jastiper(int jastiperImage, String nama, String harga) {
        this.jastiperImage = jastiperImage;
        this.nama = nama;
        this.harga = harga;
    }
}
