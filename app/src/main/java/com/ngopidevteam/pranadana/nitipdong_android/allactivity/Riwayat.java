package com.ngopidevteam.pranadana.nitipdong_android.allactivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.adapter.RiwayatAdapter;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.RiwayatModel;

import java.util.ArrayList;
import java.util.List;

public class Riwayat extends AppCompatActivity {

    private RecyclerView recRiwayat;
    private RecyclerView.Adapter riwayatAdapter;
    private List<RiwayatModel> riwayatModelList;
    private Toolbar toolbar;

    public Riwayat() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat);

       recRiwayat = findViewById(R.id.rec_riwayat);
       recRiwayat.setHasFixedSize(true);
       RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
       recRiwayat.setLayoutManager(layoutManager);
       toolbar = findViewById(R.id.toolbar);
       ImageView btnback = toolbar.findViewById(R.id.btn_back);
       TextView tvTitle = toolbar.findViewById(R.id.titlebar);
        Window window = getWindow();
        window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
        window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_purple));

       btnback.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               onBackPressed();
           }
       });

       tvTitle.setText("RIWAYAT");

       riwayatModelList = new ArrayList<>();
        riwayatModelList.add(new RiwayatModel(R.drawable.tas_pertama, "Mandala Bag","500.000", "diproses"));
        riwayatModelList.add(new RiwayatModel(R.drawable.tas_kedua, "Alea Bag","500.000", "Dibatalkan"));
        riwayatModelList.add(new RiwayatModel(R.drawable.tas_ketiga, "Mandala Bag","500.000", "diproses"));

        riwayatAdapter = new RiwayatAdapter(riwayatModelList);
        recRiwayat.setAdapter(riwayatAdapter);

    }
}
