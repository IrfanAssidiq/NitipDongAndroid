package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

import java.util.ArrayList;
import java.util.List;

public class ListDetailProfil {
    public static List<DetailProfil> detailProfilList(){

        DetailProfil akun = new DetailProfil("Akun");
        DetailProfil riwayat = new DetailProfil("Riwayat Pembelian");
        DetailProfil syarat = new DetailProfil("Syarat dan Ketentuan");
        DetailProfil kebijakan = new DetailProfil("Kebijakan Privasi");
        DetailProfil review = new DetailProfil("Review Aplikasi");
        DetailProfil bantuan = new DetailProfil("Pusat Bantuan");
        DetailProfil keluar = new DetailProfil("Keluar");

        List<DetailProfil> detailProfilList = new ArrayList<>();

        detailProfilList.add(akun);
        detailProfilList.add(riwayat);
        detailProfilList.add(syarat);
        detailProfilList.add(kebijakan);
        detailProfilList.add(review);
        detailProfilList.add(bantuan);
        detailProfilList.add(keluar);

        return detailProfilList;
    }
}
