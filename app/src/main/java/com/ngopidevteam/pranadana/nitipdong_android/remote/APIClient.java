package com.ngopidevteam.pranadana.nitipdong_android.remote;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ngopidevteam.pranadana.nitipdong_android.interceptor.TokenAuthenticator;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static final String BASE_URL="http://nitipdong.ngopidevteam.com/api/";

    private static Retrofit retrofit;

    public static Retrofit getClient(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
//                .addInterceptor(interceptor)
//                .addInterceptor(new TokenAuthenticator())
                .build();

        if (retrofit == null){

            Gson gson = new GsonBuilder()
                    .setLenient().create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }
}
