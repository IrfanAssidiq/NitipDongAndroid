package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

public class Negara {
    int imageNegara;
    String textNegara, country_id, kode;

    public Negara() {
    }

    public Negara(int imageNegara, String textNegara) {
        this.imageNegara = imageNegara;
        this.textNegara = textNegara;
    }

    public Negara(int imageNegara, String textNegara, String country_id, String kode) {
        this.imageNegara = imageNegara;
        this.textNegara = textNegara;
        this.country_id = country_id;
        this.kode = kode;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public int getImageNegara() {
        return imageNegara;
    }

    public void setImageNegara(int imageNegara) {
        this.imageNegara = imageNegara;
    }

    public String getTextNegara() {
        return textNegara;
    }

    public void setTextNegara(String textNegara) {
        this.textNegara = textNegara;
    }
}
