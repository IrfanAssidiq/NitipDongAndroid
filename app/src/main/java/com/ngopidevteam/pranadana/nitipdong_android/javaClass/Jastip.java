package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

public class Jastip {
    public int gambarProduk;
    public String namaProduk;
    public String hargaProduk;

    public String getHargaProduk() {
        return hargaProduk;
    }

    public void setHargaProduk(String hargaProduk) {
        this.hargaProduk = hargaProduk;
    }

    public Jastip(int gambarProduk, String namaProduk, String hargaProduk) {
        this.gambarProduk = gambarProduk;
        this.namaProduk = namaProduk;
        this.hargaProduk = hargaProduk;
    }

    public int getGambarProduk() {
        return gambarProduk;
    }

    public void setGambarProduk(int gambarProduk) {
        this.gambarProduk = gambarProduk;
    }

    public String getNamaProduk() {
        return namaProduk;
    }

    public void setNamaProduk(String namaProduk) {
        this.namaProduk = namaProduk;
    }
}
