package com.ngopidevteam.pranadana.nitipdong_android.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {

    public static final String SP_LOGIN_APP = "NitipDongAndroid";

    // data token from nitipdong.ngopidevteam.com/api/login
    public static final String SP_TOKEN = "spToken";
    public static final String SP_EXPIRED = "spExpired";
    public static final String SP_REFRESH = "spRefresh";

    //data user from token
    public static final String SP_ID = "spId";
    public static final String SP_USERNAME = "spUsername";
    public static final String SP_PASSWORD = "spPassword";
    public static final String SP_NAME = "spName";
    public static final String SP_EMAIL = "spEmail";
    public static final String SP_ADDRESS = "spAddress";
    public static final String SP_AGE = "spAge";
    public static final String SP_GENDER = "spGender";
    public static final int SP_USER_TYPE_ID = 0;
    public static final String SP_MERCHANT_ID = "spMerchantId";

    //data trending jastip from nitipdong.ngopidevteam.com/api/

    //add some needed function
    private static final String G_TOKEN = "firebaseToken";
    private static final String isJASTIPER ="JASTIPER";

    public static final String SP_SUDAH_LOGIN = "spSudahLogin";

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;


    public SharedPrefManager(Context context){
        sp = context.getSharedPreferences(SP_LOGIN_APP, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void saveSPString(String keySP, String value){
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPInt(String keySP, int value){
        spEditor.putInt(keySP, value);
        spEditor.commit();
    }

    public void saveSPBoolean(String keySP, boolean value){
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    public String getSPName(){
        return sp.getString(SP_NAME, "");
    }

    public static String getSpId() {
        return SP_ID;
    }

    public void setSpName(String uname){
        spEditor.putString(SP_USERNAME, uname);
        spEditor.apply();
    }
    public String getSpUsername() {
        return sp.getString(SP_USERNAME, "nameless");
    }

    public String getSpPassword() {
        return SP_PASSWORD;
    }

    public static String getSpAddress() {
        return SP_ADDRESS;
    }

    public static String getSpAge() {
        return SP_AGE;
    }

    public static String getSpGender() {
        return SP_GENDER;
    }

    public static int getSpUserTypeId() {
        return SP_USER_TYPE_ID;
    }

    public static String getSpMerchantId() {
        return SP_MERCHANT_ID;
    }

    public String getSPEmail(){
        return sp.getString(SP_EMAIL, "");
    }

    public String getSPToken(){
        return sp.getString(SP_TOKEN, "");
    }

    public Boolean getSPSudahLogin(){
        return sp.getBoolean(SP_SUDAH_LOGIN, false);
    }

    public void setGToken(String gToken){
        spEditor.putString(G_TOKEN, gToken);
        spEditor.apply();
    }
    public String getGToken(){
        return sp.getString(G_TOKEN, "token empty");
    }

    public void setIsJASTIPER(boolean status){
        spEditor.putBoolean(isJASTIPER, status);
        spEditor.apply();
    }

    public boolean getIsJASTIPER(){
        return sp.getBoolean(isJASTIPER, false);
    }
}
