package com.ngopidevteam.pranadana.nitipdong_android.remote;

import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Negara;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.NegaraResponse;
import com.ngopidevteam.pranadana.nitipdong_android.response.UserResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIService {

    @FormUrlEncoded
    @POST("login")
    Call<UserResponse> postLogin(@Field("username") String username,
                                 @Field("password") String password,
                                 @Field("apikey") String apikey);

    @FormUrlEncoded
    @POST("register")
    Call<ResponseBody> postRegister(@Field("name") String name,
                                    @Field("username") String username,
                                    @Field("email") String email,
                                    @Field("password") String password,
                                    @Field("address") String address,
                                    @Field("age") int age,
                                    @Field("gender") String gender,
                                    @Field("g_token")String token,
                                    @Field("apikey") String apikey);

    @FormUrlEncoded
    @POST("update-profile")
    Call<ResponseBody> postUpdateProfil(@Field("apikey") String apikey,
                                        @Field("username") String username,
                                        @Field("name") String name,
                                        @Field("address") String address,
                                        @Field("age") int age,
                                        @Field("gender") String gender,
                                        @Field("profile_picture") String picture);

    @FormUrlEncoded
    @POST("forgot-password")
    Call<ResponseBody> forgotPassword(@Field("apikey") String apikey,
                                      @Field("email") String email,
                                      @Field("subject") String subject);

    @FormUrlEncoded
    @POST("country")
    Call<NegaraResponse> listCountry(@Field("apikey") String apikey);

    @FormUrlEncoded
    @POST("category")
    Call<ResponseBody> listCategory(@Field("apikey") String apikey);
}
