package com.ngopidevteam.pranadana.nitipdong_android.allactivity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ngopidevteam.pranadana.nitipdong_android.R
import com.ngopidevteam.pranadana.nitipdong_android.utils.SharedPrefManager
import kotlinx.android.synthetic.main.activity_cekktp.*
import kotlinx.android.synthetic.main.appbar_other.*


/**
 *   created by Irfan Assidiq on 2020-02-20
 *   email : assidiq.irfan@gmail.com
 **/
class IdCheckActivity : AppCompatActivity() {
    lateinit var shared : SharedPrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cekktp)
        shared = SharedPrefManager(this)

        tvAmbilFoto.setOnClickListener {
            Toast.makeText(this, "under construction", Toast.LENGTH_SHORT).show()
        }
        tvAmbilFotoDiri.setOnClickListener {
            Toast.makeText(this, "under construction", Toast.LENGTH_SHORT).show()
        }
        tvkirimid.setOnClickListener {
            startActivity(Intent(this@IdCheckActivity, Homepeg::class.java))
            finish()
        }
        btn_back.setOnClickListener {
            this.onBackPressed()
        }
    }
    override fun onBackPressed() {
        shared.isJASTIPER = false
        super.onBackPressed()

    }
}