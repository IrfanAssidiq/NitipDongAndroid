package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

import androidx.annotation.Nullable;

import java.lang.reflect.Array;

import java.util.Date;
import java.util.List;

public class BaseClaim implements Claim {

    @Nullable
    @Override
    public Boolean asBoolean() {
        return null;
    }

    @Nullable
    @Override
    public Integer asInt() {
        return null;
    }

    @Nullable
    @Override
    public Long asLong() {
        return null;
    }

    @Nullable
    @Override
    public Double asDouble() {
        return null;
    }

    @Nullable
    @Override
    public String asString() {
        return null;
    }

    @Nullable
    @Override
    public Date asDate() {
        return null;
    }

    @Override
    public <T> T[] asArray(Class<T> tClass) throws DecodeException {
        return null;
    }

    @Override
    public <T> List<T> asList(Class<T> tClass) throws DecodeException {
        return null;
    }

    @Nullable
    @Override
    public <T> T asObject(Class<T> tClass) throws DecodeException {
        return null;
    }
}
