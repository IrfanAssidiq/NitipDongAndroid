package com.ngopidevteam.pranadana.nitipdong_android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.allactivity.DetailPurchaseAct;
import com.ngopidevteam.pranadana.nitipdong_android.allactivity.TrendingJastip;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.TrendingJastipModel;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class TrendingAdapter extends RecyclerView.Adapter<TrendingAdapter.ViewHolder> {

    private List<TrendingJastipModel> trendingList;
    private Context ctx;

    public TrendingAdapter(List<TrendingJastipModel> trendingList, Context ctx) {
        this.trendingList = trendingList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trending, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TrendingJastipModel trendingJastipModel = trendingList.get(position);
        final int pos = position;

        holder.imgTrending.setImageResource(trendingJastipModel.getImg_trending());
        holder.namaTrending.setText(trendingJastipModel.getNama());

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String harga = trendingJastipModel.getHarga();
        long rupiah = Long.parseLong(harga);
        holder.hargaTrending.setText(formatRupiah.format(rupiah));
        holder.llayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ctx.startActivity(new Intent(ctx, DetailPurchaseAct.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return trendingList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgTrending;
        private TextView namaTrending, hargaTrending;
        private LinearLayout llayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgTrending = itemView.findViewById(R.id.img_trending);
            namaTrending = itemView.findViewById(R.id.nama_trending);
            hargaTrending = itemView.findViewById(R.id.hrg_trending);
            llayout = itemView.findViewById(R.id.llayout);
        }
    }
}
