package com.ngopidevteam.pranadana.nitipdong_android.javaClass

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class NegaraResponse : Serializable{
    @SerializedName("status")
    val status : Long? = 0

    @SerializedName("message")
    var message : String? = null

    @SerializedName("data")
    var data : ArrayList<NegaraOKE>? = null
}