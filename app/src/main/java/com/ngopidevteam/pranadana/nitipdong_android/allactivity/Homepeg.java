package com.ngopidevteam.pranadana.nitipdong_android.allactivity;

import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.fragment.AkunFragment;
import com.ngopidevteam.pranadana.nitipdong_android.fragment.HomeFragment;
import com.ngopidevteam.pranadana.nitipdong_android.fragment.KeranjangFragment;

public class Homepeg extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private ActionBar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepeg);
        loadFragment(new HomeFragment());

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.nav);
        navigation.setOnNavigationItemSelectedListener(this);

    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null){
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();
        }
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment frag = null;
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                frag = new HomeFragment();
                break;
            case R.id.nav_keranjang:
                frag = new KeranjangFragment();
                break;
            case R.id.nav_akun:
                frag = new AkunFragment();
                break;
        }
        return loadFragment(frag);
    }
}