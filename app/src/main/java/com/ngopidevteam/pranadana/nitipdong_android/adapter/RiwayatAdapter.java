package com.ngopidevteam.pranadana.nitipdong_android.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.RiwayatModel;

import java.util.List;

public class RiwayatAdapter extends RecyclerView.Adapter<RiwayatAdapter.ViewHolder> {

    private List<RiwayatModel> riwayatList;

    public RiwayatAdapter(List<RiwayatModel> riwayatList) {
        this.riwayatList = riwayatList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_riwayat, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        RiwayatModel riwayatModel = riwayatList.get(position);
        final int pos = position;

        holder.imgRiwayatProduk.setImageResource(riwayatModel.getImg_riwayat());
        holder.namaRiwayatProduk.setText(riwayatModel.getNama());
        holder.hargaRiwayatProduk.setText(riwayatModel.getHarga());
        holder.statusRiwayatProduk.setText(riwayatModel.getStatus());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.llDropdown.getVisibility() == View.VISIBLE){
                    holder.llDropdown.setVisibility(View.GONE);
                    
                } else if (holder.llDropdown.getVisibility() == View.GONE){
                    holder.llDropdown.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return riwayatList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgRiwayatProduk;
        private TextView namaRiwayatProduk, hargaRiwayatProduk, statusRiwayatProduk;
        private LinearLayout llDropdown;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgRiwayatProduk = itemView.findViewById(R.id.img_riwayat);
            namaRiwayatProduk = itemView.findViewById(R.id.nama_riwayat);
            hargaRiwayatProduk = itemView.findViewById(R.id.harga_riwayat);
            statusRiwayatProduk = itemView.findViewById(R.id.status_riwayat);
            llDropdown = itemView.findViewById(R.id.ll_dropdown);
        }
    }
}
