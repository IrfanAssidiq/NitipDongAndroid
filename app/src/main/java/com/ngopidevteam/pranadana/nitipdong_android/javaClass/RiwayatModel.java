package com.ngopidevteam.pranadana.nitipdong_android.javaClass;

public class RiwayatModel {
    int img_riwayat;
    String nama;
    String harga;
    String status;

    public RiwayatModel(int img_riwayat, String nama, String harga, String status) {
        this.img_riwayat = img_riwayat;
        this.nama = nama;
        this.harga = harga;
        this.status = status;
    }

    public int getImg_riwayat() {
        return img_riwayat;
    }

    public void setImg_riwayat(int img_riwayat) {
        this.img_riwayat = img_riwayat;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
