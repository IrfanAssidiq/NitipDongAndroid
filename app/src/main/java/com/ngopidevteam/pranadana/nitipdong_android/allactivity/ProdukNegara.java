package com.ngopidevteam.pranadana.nitipdong_android.allactivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.adapter.ProdukNegaraAdapter;
import com.ngopidevteam.pranadana.nitipdong_android.adapter.TrendingAdapter;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.ProdukNegaraModel;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.TrendingJastipModel;

import java.util.ArrayList;
import java.util.List;

public class ProdukNegara extends AppCompatActivity {

    private RecyclerView recProduk;
    private RecyclerView.Adapter produkAdapter;
    private List<TrendingJastipModel> produkList;
    private TextView namaNegara, tvNotFound;
    ImageView btn_back_negara;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produk_negara);

        tvNotFound = findViewById(R.id.tvNotFound);
        namaNegara = findViewById(R.id.nama_negara);
        btn_back_negara = findViewById(R.id.btn_back_negara);
        String ambilNamaNegara = getIntent().getStringExtra("nama");
        namaNegara.setText(ambilNamaNegara);

        Window window = getWindow();
        recProduk = findViewById(R.id.rec_produk_negara);
        recProduk.setHasFixedSize(true);

        if(ambilNamaNegara.equals("Italia")){
            tvNotFound.setVisibility(View.GONE);
            recProduk.setVisibility(View.VISIBLE);
        }else{
            tvNotFound.setVisibility(View.VISIBLE);
            tvNotFound.setText("belum ada jastiper di "+ambilNamaNegara);
            recProduk.setVisibility(View.GONE);
        }

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recProduk.setLayoutManager(gridLayoutManager);
        window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
        window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_purple));

        produkList = new ArrayList<>();
        produkList.add(new TrendingJastipModel(R.drawable.tas_pertama, "Mandala Bag", "500000"));
        produkList.add(new TrendingJastipModel(R.drawable.tas_kedua, "Gucci Bag", "1000000"));
        produkList.add(new TrendingJastipModel(R.drawable.tas_ketiga, "Bruno Bag", "750000"));
        produkList.add(new TrendingJastipModel(R.drawable.tas_keempat, "Alea Bag", "5000000"));
        produkList.add(new TrendingJastipModel(R.drawable.tas_kelima, "Karina Bag", "6000000"));

        produkAdapter = new TrendingAdapter(produkList, this);
        recProduk.setAdapter(produkAdapter);

        btn_back_negara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
