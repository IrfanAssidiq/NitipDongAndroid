package com.ngopidevteam.pranadana.nitipdong_android.allactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.ngopidevteam.pranadana.nitipdong_android.R

class SplashScreen : AppCompatActivity() {

    lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        val window = window
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN


        handler = Handler()
        handler.postDelayed({
            val intent = Intent(this@SplashScreen, Loggin::class.java)
            startActivity(intent)
            finish()
        }, 3000)
    }
}
