package com.ngopidevteam.pranadana.nitipdong_android.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ngopidevteam.pranadana.nitipdong_android.R;
import com.ngopidevteam.pranadana.nitipdong_android.allactivity.TrendingJastip;
import com.ngopidevteam.pranadana.nitipdong_android.javaClass.Kategori;

import java.util.List;

public class KategoriHorizontalAdapter extends RecyclerView.Adapter<KategoriHorizontalAdapter.ViewHolder> {

    private List<Kategori> horizontalKategori;
    Context context;
    Kategori kategori;

    public KategoriHorizontalAdapter(List<Kategori> kategoriList, Context context) {
        this.horizontalKategori = kategoriList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View productView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_category, viewGroup, false);
        ViewHolder kvh = new ViewHolder(productView);
        return kvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        kategori = horizontalKategori.get(i);
        viewHolder.imgKategori.setImageResource(kategori.getGambarKategori());
        viewHolder.tv_category.setText(kategori.getTextOke());
        viewHolder.imgKategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, ""+horizontalKategori.get(i).getTextOke(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, TrendingJastip.class);
                intent.putExtra("kategori",horizontalKategori.get(i).getTextOke());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return horizontalKategori.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imgKategori;
        TextView tv_category;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgKategori = itemView.findViewById(R.id.img_kategori);
            tv_category = itemView.findViewById(R.id.tv_category);
        }
    }
}
